import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return [{
      id: '1',
      titulo: 'Fortnite',
      plataforma: 'Play Station 4',
      desarrollador: 'Epic Games',
      precio: '₡50 000',
      genero: 'BattleRoyal',
      image: 'https://http2.mlstatic.com/fortnite-ps4-fisico-deluxe-founders-pack-nuevo-sellado-D_NQ_NP_917886-MLA26883927212_022018-F.jpg'
    }, {
      id: '2',
      titulo: 'God of War',
      plataforma: 'Play Station 4',
      desarrollador: 'Santa Monica Studios',
      precio: '₡50 000',
      genero: 'Aventura',
      image: 'https://http2.mlstatic.com/god-of-war-ps4-gow-fisico-nuevo-nextgames-D_NQ_NP_783521-MLA27172871700_042018-F.jpg'
    }, {
      id: '3',
      titulo: 'Doom',
      plataforma: 'Xbox One',
      desarrollador: 'Bethesda',
      precio: '₡50 000',
      genero: 'Shooters',
      image: 'https://http2.mlstatic.com/doom-xbox-one-juego-nuevo-fisico-D_NQ_NP_491225-MCO25393575761_022017-F.jpg'
    }, {
      id: '4',
      titulo: 'The legend of Zelda: Breath of the Wild',
      plataforma: 'Nintendo Switch',
      desarrollador: 'Nintendo',
      precio: '₡50 000',
      genero: 'Aventura',
      image: 'https://http2.mlstatic.com/the-legend-of-zelda-breath-of-the-wild-switch-fisico-nuevo-D_NQ_NP_913960-MLA27146658958_042018-F.jpg'
    }];
  }
});
